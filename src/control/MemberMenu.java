package control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import model.PersonList;
import model.SubjectList;
import print.MemberMenuPrint;
import value.Employee;
import value.Student;
import value.Teacher;

public class MemberMenu {
	
	MemberMenu memberMenu = new MemberMenu();
	private static Scanner in = new Scanner(System.in);
	private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	
	public void startMenu() {
		MemberMenuPrint.print();
		
		int selectMenu = in.nextInt();
		in.nextLine();
		
		switch(selectMenu) {
			case 1:
				memberMenu.insert();
				break;
			case 2:
				memberMenu.modify();
				break;
			case 3:
				memberMenu.delete();
				break;
			case 4:
				memberMenu.check();
				break;
			case 99:
				MainMenu.start();
				break;
		}
	}
	
	public void insert() {
		MemberMenuPrint.insertPrint();
		
		int selectMenu;
		String inputName, inputDate, inputSubject;
		Date date;
		
		selectMenu = in.nextInt();
		in.nextLine();
		
		switch(selectMenu) {
			case 1: //insert student information in PersonList
				MemberMenuPrint.insertNamePrint("학생");
				inputName = in.nextLine();
				MemberMenuPrint.insertDatePrint("학생");
				inputDate = in.nextLine();
				
				try {
					date = format.parse(inputDate);
					Student student = new Student(inputName, date);
					PersonList.insertPerson(student);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				break;
			case 2:	//insert employee information in PersonList
				MemberMenuPrint.insertNamePrint("교직원");
				inputName = in.nextLine();
				MemberMenuPrint.insertDatePrint("교직원");
				inputDate = in.nextLine();
				
				try {
					date = format.parse(inputDate);
					Employee employee = new Employee(inputName, date);
					PersonList.insertPerson(employee);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				break;
			case 3: //insert teacher information in PersonList
				MemberMenuPrint.insertNamePrint("선생님");
				inputName = in.nextLine();
				MemberMenuPrint.insertDatePrint("선생님");
				inputDate = in.nextLine();
				
				try {
					date = format.parse(inputDate);
					MemberMenuPrint.insertSubjectPrint();
					inputSubject = in.nextLine();
					
					if(SubjectList.getSubject(inputSubject) != null) {
						Teacher teacher = new Teacher(inputName, date, inputSubject);
						PersonList.insertPerson(teacher);
					}
					else {
						System.out.println("개설된 과목이 아니라 생성되지 않습니다.");
					}
				} catch (ParseException e1) {
					System.out.println("생년월일의 형식이 맞지 않습니다");
				}
				
				break;
			case 99:
				break;
		}
		memberMenu.startMenu();
	}
	
	public void modify() {
		MemberMenuPrint.modifyPrint();
		
		int selectMenu;
		String inputName, inputId, inputDate, inputSubject;
		Date date;
		
		selectMenu = in.nextInt();
		in.nextLine();
		
		switch(selectMenu) {
			case 1: //modify name information
				inputName = in.nextLine();
				if(MemberMenuPrint.personPrintByName(inputName)) {
					inputId = in.nextLine();
					
					if(PersonList.getPerson(inputId) != null) {
						MemberMenuPrint.modifyNamePrint();
						inputName = in.nextLine();
						
						PersonList.getPerson(inputId).setName(inputName);
					}
					else {
						System.out.println("존재하지 않는 인원입니다");
					}
				}
				else {
					System.out.println("존재하지 않는 인원입니다");
				}
				break;
			case 2: //modify date information
				inputName = in.nextLine();
				if(MemberMenuPrint.personPrintByName(inputName)) {
					inputId = in.nextLine();
					
					if(PersonList.getPerson(inputId) != null) {
						MemberMenuPrint.modifyDatePrint();
						
						inputDate = in.nextLine();
						try {
							date = format.parse(inputDate);
							PersonList.getPerson(inputId).setDate(date);
						} catch (ParseException e) {
							System.out.println("올바르지 않은 생년월일 형식입니다");
						}
					}
					else {
						System.out.println("존재하지 않는 인원입니다");
					}
				}
				else {
					System.out.println("존재하지 않는 인원입니다");
				}
				break;
			case 3: //modify subject information
				inputName = in.nextLine();
				if(MemberMenuPrint.personPrintByName(inputName)) {
					inputId = in.nextLine();
					
					if(inputId.substring(1).equals("T") && PersonList.getPerson(inputId) != null) {
						MemberMenuPrint.modifySubjectPrint();
						inputSubject = in.nextLine();
						
						((Teacher)PersonList.getPerson(inputId)).setSubject(inputSubject);
					}
					else {
						System.out.println("존재하지 않는 인원이거나 선생님이 아닙니다");
					}
				}
				else {
					System.out.println("존재하지 않는 인원입니다");
				}
				break;
			case 99:
				break;
		}
		
		memberMenu.startMenu();
	}
	
	public void delete() {
		MemberMenuPrint.deletePrint();
		
		String inputName, inputId;
		
		inputName = in.nextLine();
		
		if(MemberMenuPrint.personPrintByName(inputName)){
			inputId = in.nextLine();
			
			if(PersonList.getPerson(inputId) != null) {
				PersonList.deletePerson(inputId);
			}
			else {
				System.out.println("존재하지 않는 인원입니다");
			}
		}
		else {
			System.out.println("존재하지 않는 인원입니다");
		}
		
		memberMenu.startMenu();
	}
	
	public void check() {
		MemberMenuPrint.checkPrint();
		memberMenu.startMenu();
	}
}
