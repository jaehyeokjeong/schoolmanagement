package control;

import java.util.Scanner;

import print.MainMenuPrint;

public class MainMenu {
	
	public static void start() {
		MainMenuPrint.print();
		
		MemberMenu memberMenu = new MemberMenu();
		SubjectMenu subjectMenu = new SubjectMenu();
		GradeMenu gradeMenu = new GradeMenu();
		
		Scanner in = new Scanner(System.in);
		
		int selectMenu = in.nextInt();
		in.nextLine();
		
		switch(selectMenu) {
			case 1:
				memberMenu.startMenu();
				break;
			case 2:
				subjectMenu.startMenu();
				break;
			case 3:
				gradeMenu.startMenu();
				break;
			case 99:
				System.out.println("프로그램을 종료합니다");
				System.exit(0);
		}
	}
}
