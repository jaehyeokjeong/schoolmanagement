package control;

import java.util.Scanner;

import model.GradeList;
import model.PersonList;
import model.SubjectList;
import print.GradeMenuPrint;
import value.Grade;

public class GradeMenu {
	
	GradeMenu gradeMenu = new GradeMenu();
	private static Scanner in = new Scanner(System.in);
	
	public void startMenu() {
		GradeMenuPrint.print();
		
		int selectMenu = in.nextInt();
		in.nextLine();
		
		switch(selectMenu) {
			case 1:
				gradeMenu.insert();
				break;
			case 2:
				gradeMenu.modify();
				break;
			case 3:
				gradeMenu.delete();
				break;
			case 4:
				gradeMenu.check();
				break;
			case 99:
				MainMenu.start();
				break;
		}
	}
	
	public void insert() {
		GradeMenuPrint.insertPrint();
		
		String inputId, inputName, inputSubject;
		int score;
		
		inputName = in.nextLine();
		if(GradeMenuPrint.personPrintByName(inputName)){
			inputId = in.nextLine();
			
			if(PersonList.getPerson(inputId) != null) {
				System.out.println("		과목명 입력");
				inputSubject = in.nextLine();
				
				if(SubjectList.getSubject(inputSubject) != null) {
					System.out.println("		점수 입력");
					score = in.nextInt();
					in.nextLine();
					
					Grade grade = new Grade(inputId, inputName, inputSubject, score);
					GradeList.insertGrade(grade);
				}
				else {
					System.out.println("개설되지 않은 과목입니다");
				}
			}
			else {
				System.out.println("존재하지 않는 인원입니다");
			}
		}
		else {
			System.out.println("존재하지 않는 인원입니다");
		}
		
		gradeMenu.startMenu();
	}
	
	public void modify() {
		GradeMenuPrint.modifyPrint();
		
		String inputId, inputName, inputSubject;
		int inputScore;
		
		inputName = in.nextLine();
		if(GradeMenuPrint.personPrintByName(inputName)){
			inputId = in.nextLine();
			
			if(PersonList.getPerson(inputId) != null) {
				GradeMenuPrint.gradePrintById(inputId, inputName, "수정", false);
				inputSubject = in.nextLine();
				
				if(SubjectList.getSubject(inputSubject) != null) {
					System.out.println("		점수 입력");
					inputScore = in.nextInt();
					in.nextLine();
					
					Grade grade = new Grade(inputId, inputName, inputSubject, inputScore);
					GradeList.insertGrade(grade);
				}
				else {
					System.out.println("개설되지 않은 과목입니다");
				}
			}
			else {
				System.out.println("존재하지 않는 인원입니다");
			}
		}
		else {
			System.out.println("존재하지 않는 인원입니다");
		}
		
		gradeMenu.startMenu();
	}
	
	public void delete() {
		GradeMenuPrint.deletePrint();
		
		String inputId, inputName, inputSubject;
		
		inputName = in.nextLine();
		if(GradeMenuPrint.personPrintByName(inputName)){
			inputId = in.nextLine();
			
			if(PersonList.getPerson(inputId) != null) {
				GradeMenuPrint.gradePrintById(inputId, inputName, "삭제", false);
				inputSubject = in.nextLine();
				
				if(SubjectList.getSubject(inputSubject) != null) {
					GradeList.deleteGrade(inputId, inputSubject);
				}
				else {
					System.out.println("개설되지 않은 과목입니다");
				}
			}
			else {
				System.out.println("존재하지 않는 인원입니다");
			}
		}
		else {
			System.out.println("존재하지 않는 인원입니다");
		}
		
		gradeMenu.startMenu();
	}
	
	public void check() {
		GradeMenuPrint.checkPrint();
		
		String inputId, inputName, inputSubject;
		int selectMenu;

		selectMenu = in.nextInt();
		in.nextLine();
		switch(selectMenu) {
			case 1: //전체 조회
				GradeMenuPrint.gradePrint();
				break;
			case 2: //학생별 조회 (평균 포함)
				System.out.println("		학생 이름 입력");
				inputName = in.nextLine();
				if(GradeMenuPrint.personPrintByName(inputName)){
					inputId = in.nextLine();
					
					if(PersonList.getPerson(inputId) != null) {
						GradeMenuPrint.gradePrintById(inputId, inputName, "조회", true);
					}
					else {
						System.out.println("존재하지 않는 인원입니다");
					}
				}
				else {
					System.out.println("존재하지 않는 인원입니다");
				}
				break;
			case 3: //과목별 조회 (평균 포함)
				inputSubject = in.nextLine();
				if(SubjectList.getSubject(inputSubject) != null) {
					GradeMenuPrint.gradePrintBySubject(inputSubject, true);
				}
				else {
					System.out.println("존재하지 않는 과목입니다");
				}
				break;
			case 99:
				gradeMenu.startMenu();
				break;
		}
	}
}
