package control;

import java.util.Scanner;

import model.SubjectList;
import print.SubjectMenuPrint;
import value.Subject;

public class SubjectMenu {
	
	SubjectMenu subjectMenu = new SubjectMenu();
	private static Scanner in = new Scanner(System.in);
	
	public void startMenu() {
		SubjectMenuPrint.print();
		
		int selectMenu = in.nextInt();
		in.nextLine();
		
		switch(selectMenu) {
			case 1:
				subjectMenu.insert();
				break;
			case 2:
				subjectMenu.modify();
				break;
			case 3:
				subjectMenu.delete();
				break;
			case 4:
				subjectMenu.check();
				break;
			case 99:
				MainMenu.start();
				break;
		}
	}
	
	public void insert() {
		SubjectMenuPrint.insertPrint();
		
		String inputSubject = in.nextLine();
		
		if(SubjectList.getSubject(inputSubject) == null) {
			Subject subject = new Subject(inputSubject);
			SubjectList.insertSubject(subject);
			System.out.println("과목이 생성되었습니다");
		}
		else {
			System.out.println("동일 과목명이 존재합니다");
		}
		
		
		subjectMenu.startMenu();
	}
	
	public void modify() {
		SubjectMenuPrint.modifyPrint();
		
		String inputSubject = in.nextLine();
		
		if(SubjectList.getSubject(inputSubject) != null) {
			SubjectMenuPrint.modifyPrint2();
			String modifySubject = in.nextLine();
			
			if(SubjectList.getSubject(modifySubject) == null) {
				SubjectList.getSubject(inputSubject).setName(modifySubject);
				System.out.println("과목명이 수정되었습니다");
			}
			else {
				System.out.println("동일 과목명이 존재합니다");
			}
		}
		else {
			System.out.println("존재하지 않는 과목입니다");
		}
		
		subjectMenu.startMenu();
	}
	
	public void delete() {
		SubjectMenuPrint.deletePrint();
		
		String deleteSubject = in.nextLine();
		
		if(SubjectList.getSubject(deleteSubject) != null) {
			SubjectList.deleteSubject(deleteSubject);
			System.out.println("과목이 삭제되었습니다");
		}
		else {
			System.out.println("존재하지 않는 과목입니다");
		}
		
		subjectMenu.startMenu();
	}
	
	public void check() {
		SubjectMenuPrint.checkPrint();
		subjectMenu.startMenu();
	}
}
