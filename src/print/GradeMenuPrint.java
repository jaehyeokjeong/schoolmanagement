package print;

import java.text.DecimalFormat;
import java.util.ArrayList;

import model.GradeList;
import model.PersonList;
import value.Grade;
import value.Person;

public class GradeMenuPrint {
	public static void print() {
		System.out.println("----------------------------------------");
		System.out.println("		성적관리");
		System.out.println("----------------------------------------");
		System.out.println("1. 성적 입력");
		System.out.println("2. 성적 수정");
		System.out.println("3. 성적 삭제");
		System.out.println("4. 성적 조회");
		System.out.println("99. 이전 메뉴로");
		System.out.println("----------------------------------------");
	}
	
	public static void insertPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	성적입력 (입력할 학생의 이름을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void modifyPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	성적수정 (수정할 학생의 이름을 입력하세요)");
		System.out.println("----------------------------------------");
	}

	public static void deletePrint() {
		System.out.println("----------------------------------------");
		System.out.println("	성적삭제 (삭제할 학생의 이름을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void checkPrint() {
		System.out.println("----------------------------------------");
		System.out.println("		성적조회");
		System.out.println("1. 전체 조회");
		System.out.println("2. 학생별 조회");
		System.out.println("3. 과목별 조회");
		System.out.println("99. 이전 메뉴로");
		System.out.println("----------------------------------------");
	}
	
	public static void gradePrint() {
		System.out.println("		모든 학생의 성적입니다");
		for(int i=0; i<GradeList.getGradeList().size(); i++) {
			System.out.println(GradeList.getGradeList().get(i).toString());
		}
	}
	
	public static void gradePrintById(String id, String name, String menu, boolean avgPrint) {
		double avg = 0;
		String pattern = ".##";
		DecimalFormat dformat = new DecimalFormat(pattern);
		
		if(menu.equals("조회")) {
			System.out.println("		[" + id + "] " + name + "의 성적입니다");
		}
		else {
			System.out.println("		" + menu + "과목 입력");
		}
		
		ArrayList<Grade> returnList = GradeList.getGradeListById(id);
		for(int i=0; i < returnList.size(); i++) {
			System.out.println(returnList.get(i).toString());
			avg += returnList.get(i).getScore();
		}
		
		if(avgPrint) {
			System.out.println("평균: " + dformat.format(avg/returnList.size()));
		}
	}
	
	public static void gradePrintBySubject(String subject, boolean avgPrint) {
		double avg = 0;
		String pattern = ".##";
		DecimalFormat dformat = new DecimalFormat(pattern);
		
		ArrayList<Grade> returnList = GradeList.getGradeListBySubject(subject);
		for(int i=0; i < returnList.size(); i++) {
			System.out.println(returnList.get(i).toString());
			avg += returnList.get(i).getScore();
		}
		
		if(avgPrint) {
			System.out.println("평균: " + dformat.format(avg/returnList.size()));
		}
	}
	
	public static boolean personPrintByName(String name) {
		int check = 0;
		
		ArrayList<Person> returnList = PersonList.getPersonListByName(name);
		for(int i=0; i < returnList.size(); i++) {
			check++;
			System.out.println(returnList.get(i).toString());
		}
		System.out.println("----------------------------------------");
		if(check != 0) {
			System.out.println("	이름으로 검색된 인원의 id를 입력하세요");
			return true;
		}
		else {
			System.out.println("	조회된 인원이 없습니다");
			return false;
		}
	}
}
