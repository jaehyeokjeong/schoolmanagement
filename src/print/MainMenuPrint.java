package print;

public class MainMenuPrint {
	public static void print() {
		System.out.println("----------------------------------------");
		System.out.println("	학교 인원 & 성적 관리 시스템	");
		System.out.println("----------------------------------------");
		System.out.println("1. 인원 관리");
		System.out.println("2. 과목 관리");
		System.out.println("3. 성적 관리");
		System.out.println("99. 프로그램 종료");
		System.out.println("----------------------------------------");
	}
}
