package print;

import java.util.ArrayList;

import model.SubjectList;
import value.Subject;

public class SubjectMenuPrint {
	public static void print() {
		System.out.println("----------------------------------------");
		System.out.println("		과목관리");
		System.out.println("----------------------------------------");
		System.out.println("1. 과목 입력");
		System.out.println("2. 과목 수정");
		System.out.println("3. 과목 삭제");
		System.out.println("4. 과목 조회");
		System.out.println("99. 이전 메뉴로");
		System.out.println("----------------------------------------");
	}
	
	public static void insertPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	과목입력 (생성할 과목명을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void modifyPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	과목수정 (수정할 과목명을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void modifyPrint2() {
		System.out.println("----------------------------------------");
		System.out.println("	과목수정 (수정된 과목명을 입력하세요)");
		System.out.println("----------------------------------------");
	}

	public static void deletePrint() {
		System.out.println("----------------------------------------");
		System.out.println("	과목삭제 (삭제할 과목명을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void checkPrint() {
		System.out.println("----------------------------------------");
		System.out.println("		과목조회");
		System.out.println("----------------------------------------");
		
		ArrayList<Subject> returnList = SubjectList.getSubjectList();
		
		for(int i=0; i < returnList.size(); i++) {
			System.out.println(returnList.get(i).toString());
		}
	}
}
