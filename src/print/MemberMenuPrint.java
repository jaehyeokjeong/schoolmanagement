package print;

import java.util.ArrayList;

import model.PersonList;
import value.Person;

public class MemberMenuPrint {
	public static void print() {
		System.out.println("----------------------------------------");
		System.out.println("		인원관리");
		System.out.println("----------------------------------------");
		System.out.println("1. 인원 생성");
		System.out.println("2. 인원 수정");
		System.out.println("3. 인원 삭제");
		System.out.println("4. 인원 조회");
		System.out.println("99. 이전 메뉴로");
		System.out.println("----------------------------------------");
	}
	
	public static void insertPrint() {
		System.out.println("----------------------------------------");
		System.out.println("		인원생성");
		System.out.println("----------------------------------------");
		System.out.println("1. 학생 생성");
		System.out.println("2. 교직원 생성");
		System.out.println("3. 선생님 생성");
		System.out.println("99. 이전 메뉴로");
		System.out.println("----------------------------------------");
	}
	
	public static void insertNamePrint(String s) {
		System.out.println("----------------------------------------");
		System.out.println("	" + s + "생성 (이름을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void insertDatePrint(String s) {
		System.out.println("----------------------------------------");
		System.out.println("	" + s + "생성 (yyyy-MM-dd형식으로 생년월일을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void insertSubjectPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	선생님생성 (생성할 과목명을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void modifyPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	인원수정 (수정할 정보를 선택하세요)");
		System.out.println("----------------------------------------");
		System.out.println("1. 이름 수정");
		System.out.println("2. 생년월일 수정");
		System.out.println("3. 과목 수정");
		System.out.println("99. 이전 메뉴로");
		System.out.println("----------------------------------------");
	}
	
	public static void modifyNamePrint() {
		System.out.println("----------------------------------------");
		System.out.println("	인원수정 (수정된 이름을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void modifyDatePrint() {
		System.out.println("----------------------------------------");
		System.out.println("	인원수정 (yyyy-MM-dd형식으로 수정된 생년월일을 입력하세요)");
		System.out.println("----------------------------------------");
	}
	
	public static void modifySubjectPrint() {
		System.out.println("----------------------------------------");
		System.out.println("	인원수정 (수정된 과목 입력하세요)");
		System.out.println("----------------------------------------");
	}

	public static void deletePrint() {
		System.out.println("----------------------------------------");
		System.out.println("		인원삭제");
		System.out.println("----------------------------------------");
	}
	
	public static void checkPrint() {
		System.out.println("----------------------------------------");
		System.out.println("		인원조회");
		System.out.println("----------------------------------------");
		
		ArrayList<Person> returnList = PersonList.getPersonList();
		
		for(int i=0; i < returnList.size(); i++) {
			System.out.println(returnList.get(i).toString());
		}
	}
	
	public static boolean personPrintByName(String name) {
		int check = 0;
		
		ArrayList<Person> returnList = PersonList.getPersonListByName(name);
		for(int i=0; i < returnList.size(); i++) {
			check++;
			System.out.println(returnList.get(i).toString());
		}
		System.out.println("----------------------------------------");
		if(check != 0) {
			System.out.println("	이름으로 검색된 인원의 id를 입력하세요");
			return true;
		}
		else {
			System.out.println("	조회된 인원이 없습니다");
			return false;
		}
	}
}
