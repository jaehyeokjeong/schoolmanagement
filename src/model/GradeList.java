package model;

import java.util.ArrayList;
import java.util.Iterator;

import value.Grade;

public class GradeList {
	private static ArrayList<Grade> gradeList = new ArrayList<Grade>();
	
	private static final GradeList gl = new GradeList();
	
	private GradeList() {}
	
	public static GradeList getInstance() {
		return gl;
	}
	
	public static ArrayList<Grade> getGradeList() {
		return gradeList;
	}
	
	public static void insertGrade(Grade grade) {
		gradeList.add(grade);
	}
	
	public static ArrayList<Grade> getGradeListById(String id) {
		ArrayList<Grade> returnList = new ArrayList<Grade>();
		for(int i=0; i < gradeList.size(); i++) {
			if(gradeList.get(i).getStudentId().equals(id)) {
				returnList.add(gradeList.get(i));
			}
		}
		
		return returnList;
	}
	
	public static ArrayList<Grade> getGradeListBySubject(String subject) {
		ArrayList<Grade> returnList = new ArrayList<Grade>();
		for(int i=0; i < gradeList.size(); i++) {
			if(gradeList.get(i).getSubjectName().equals(subject)) {
				returnList.add(gradeList.get(i));
			}
		}
		
		return returnList;
	}
	
	public static Grade getGrade(String name, String subject) {
		for(int i=0; i<gradeList.size(); i++) {
			if(gradeList.get(i).getStudentName().equals(name) && gradeList.get(i).getSubjectName().equals(subject)) {
				return gradeList.get(i);
			}
		}
		return null;
	}
	
	public static void deleteGrade(String id, String subject) {
		Iterator<Grade> iter = gradeList.iterator();
		
		while(iter.hasNext()) {
			Grade tempGrade = iter.next();
			
			if(tempGrade.getStudentId().equals(id) && tempGrade.getSubjectName().equals(subject)) {
				iter.remove();
			}
		}
	}
}

