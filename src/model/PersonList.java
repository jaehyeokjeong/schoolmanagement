package model;

import java.util.ArrayList;
import java.util.Iterator;

import value.Person;

public class PersonList {
	private static ArrayList<Person> personList = new ArrayList<Person>();
	
	private static final PersonList pl = new PersonList();
	
	private PersonList() {}
	
	public static PersonList getInstance() {
		return pl;
	}
	
	public static Person getPerson(String id) {
		for(int i=0; i < personList.size(); i++) {
			if(personList.get(i).getId().equals(id)) {
				return personList.get(i);
			}
		}
		return null;
	}
	
	public static ArrayList<Person> getPersonList() {
		return personList;
	}
	
	public static void insertPerson(Person person) {
		personList.add(person);
	}
	
	public static void deletePerson(String id) {
		Iterator<Person> iter = personList.iterator();
		
		while(iter.hasNext()) {
			Person tempPerson = iter.next();
			
			if(tempPerson.getId().equals(id)) {
				iter.remove();
			}
		}
	}

	public static ArrayList<Person> getPersonListByName(String name) {
		ArrayList<Person> returnList = new ArrayList<Person>();
		for(int i=0; i < personList.size(); i++) {
			if(personList.get(i).getName().equals(name)) {
				returnList.add(personList.get(i));
			}
		}
		
		return returnList;
	}
}
