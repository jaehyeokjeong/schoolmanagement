package model;

import java.util.ArrayList;
import java.util.Iterator;

import value.Subject;

public class SubjectList {
	private static ArrayList<Subject> subjectList = new ArrayList<Subject>();
	
	private static final SubjectList sl = new SubjectList();
	
	private SubjectList() {}
	
	public static SubjectList getInstance() {
		return sl;
	}
	
	public static ArrayList<Subject> getSubjectList() {
		return subjectList;
	}
	
	public static Subject getSubject(String subjectName) {
		for(int i=0; i<subjectList.size(); i++) {
			if(subjectList.get(i).getName().equals(subjectName)) {
				return subjectList.get(i);
			}
		}
		return null;
	}
	
	public static void insertSubject(Subject subject) {
		subjectList.add(subject);
	}
	public static void deleteSubject(String subjectName) {
		Iterator<Subject> iter = subjectList.iterator();
		
		while(iter.hasNext()) {
			Subject tempSubject = iter.next();
			
			if(tempSubject.getName().equals(subjectName)) {
				iter.remove();
			}
		}
	}
}
