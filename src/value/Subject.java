package value;

public class Subject {
	private static int count = 0;
	private String id;
	private String name;
	
	public Subject(String name) {
		id = "SUB" + (++count);
		this.name = name;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setName(String name) { 
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "[" + id + "] Subject name: " + name;
	}
}
