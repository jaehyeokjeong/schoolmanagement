package value;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Person{
	protected String id;
	protected String name;
	protected Date date;
	
	public Person(String id, String name, Date date) {
		this.id = id;
		this.name = name;
		this.date = date;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setName(String name) { 
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return "[" + id + "] Name: " + name + " / birthDate: " + dateFormat.format(date);
	}

}
