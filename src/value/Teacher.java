package value;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Teacher extends Person{
	private static int count = 0;
	private String subject;

	public Teacher(String name, Date date, String subject) {
		super("TCR"+(++count), name, date);
		this.subject = subject;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return "[" + id + "] Name: " + name + " / birthDate: " + dateFormat.format(date) + " / Subject: " + subject;
	}
	
}
