package value;

import java.util.Date;

public class Employee extends Person{
	private static int count = 0;

	public Employee(String name, Date date) {
		super("EMP"+(++count), name, date);
	}
}
