package value;

import java.util.Date;

public class Student extends Person{
	private static int count = 0;
	
	public Student(String name, Date date) {
		super("STU"+(++count), name, date);
	}
	
}
