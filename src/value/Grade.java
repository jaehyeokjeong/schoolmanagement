package value;

public class Grade {
	private String studentId;
	private String studentName;
	private String subjectName;
	private int score;
	
	public Grade(String studentId, String studentName, String subjectName, int score) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.subjectName = subjectName;
		this.score = score;
	}
	
	public String getStudentId() {
		return studentId;
	}
	
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	
	public String getStudentName() {
		return studentName;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	
	public String getSubjectName() {
		return subjectName;
	}
	
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public String toString() {
		return "[" + studentId + "] Name: " + studentName + " / Subject: " + subjectName + " = " + score; 
	}
}
